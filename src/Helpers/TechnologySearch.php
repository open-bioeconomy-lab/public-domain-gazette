<?php

namespace Outlandish\Website\Helpers;

use Outlandish\Website\PostTypes\Technology;
use Outlandish\Wordpress\Oowp\OowpQuery;

class TechnologySearch
{
    /** @var array */
    protected $args;

    /** @var OowpQuery|Technology[] */
    protected $results;

    /**
     * @param array $args
     */
    public function __construct($args)
    {
        $this->args = ['s' => null, 'type' => null, 'category' => null];
        foreach ($this->args as $key => $value) {
            if (!empty($args[$key])) {
                $this->args[$key] = $args[$key];
            }
        }
    }

    public function hasQuery()
    {
        return !!array_filter($this->args);
    }

    public function getSearchTerm()
    {
        return $this->args['s'];
    }

    public function getArgs()
    {
        return $this->args;
    }

    public function getSearchResults()
    {
        if (!$this->results) {
            $this->findTechnologies();
        }
        return $this->results;
    }

    public function findTechnologies()
    {
        if ($this->results || !$this->hasQuery()) {
            return;
        }

        $searchTerm = '';

        if (!empty($this->args['s'])) {
            $searchTerm = sanitize_text_field(stripslashes(urldecode($this->args['s'])));
        }


        $metaQuery = ['relation' => 'AND'];
        $taxQuery = ['relation' => 'AND'];

        if (!empty($this->args['type'])) {
            // LIKE seems to be the best way to do this according to
            // https://www.advancedcustomfields.com/resources/query-posts-custom-fields/
            $metaQuery[] = [
                'key'       => 'type',
                'value'     => $this->args['type'], // should handle multi (array) or single, and sanitise for us
                'compare'   => 'LIKE'
            ];
        }

        if (!empty($this->args['category'])) {
            $taxQuery[] = [
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => $this->args['category'],
            ];
        }

        $this->results = Technology::fetchAll([
            's' => $searchTerm ,
            'meta_query' => $metaQuery,
            'tax_query' => $taxQuery,
        ]);
    }
}
