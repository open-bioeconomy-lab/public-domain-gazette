<?php

namespace Outlandish\Website\PostTypes;

use Outlandish\Wordpress\Oowp\Util\ArrayHelper;

class Technology extends BasePost
{
    const TECH_INVENTOR_CONNECTION = 'tech_inventor';

    public static function friendlyNamePlural(): string
    {
        return 'Technologies';
    }

    public function breadcrumbTrail($includeSelf = true): array
    {
        $trail = [
            [home_url(), 'Home'],
        ];
        if ($includeSelf) {
            $trail[] = $this->title();
        }

        return $trail;
    }

    public static function onRegistrationComplete(): void
    {
        static::registerCategoryTaxonomy();

        self::registerConnection(Inventor::postType(), [
            'sortable' => 'any',
            'cardinality' => 'many-to-one',
            'prevent_duplicates' => true,
            'title' => ['from' => 'Inventor', 'to' => 'Inventions'],
        ], static::TECH_INVENTOR_CONNECTION);

        self::registerConnection(self::postType(), [
            'sortable' => 'any',
            'cardinality' => 'many-to-many',
            'prevent_duplicates' => true,
            'title' => 'Technologies',
        ]);
    }

    public static function getRegistrationArgs()
    {
        return array_merge(
            parent::getRegistrationArgs(),
            [
                'menu_icon' => 'dashicons-media-text',
                'menu_position' => 20,
                'map_meta_cap' => true,
            ]
        );
    }

    public function abstract(): ?string
    {
        return $this->metadata('abstract');
    }

    /**
     * @return string|null  Hex colour code if applicable, otherwise null
     */
    public function uvColour(): ?string
    {
        return $this->metadata('uv_colour');
    }

    public function footnote(): ?string
    {
        return $this->metadata('footnote');
    }

    public function isPatent(): bool
    {
        return ($this->metadata('type') === 'patent');
    }

    /**
     * @return string|null  Optional WYSIWYG content for display alongside key info, on home page feature block or similar.
     */
    public function featureContent(): ?string
    {
        return $this->metadata('feature_content');
    }

    public function lensDnaSequenceUrl(): ?string
    {
        return $this->metadata('lens_dna_sequence_url');
    }

    public function lensFamilyUrl(): ?string
    {
        return $this->metadata('lens_family_url');
    }

    public function lensUrl(): ?string
    {
        return $this->metadata('lens_url');
    }

    /**
     * @return string[]
     */
    public function openUseFlags(): array
    {
        return $this->metadata('open_use_flags', false);
    }

    public function patentNumber(): ?string
    {
        return $this->metadata('us_patent_number');
    }

    public function patentTitle(): ?string
    {
        return $this->metadata('us_patent_title');
    }

    public function publicDate($format = 'j M Y'): ?string
    {
        if ($this->isPatent()) {
            $date = $this->metadata('expiry_date');
        } else {
            $date = $this->metadata('public_domain_since');
        }
        if ($date && $format) {
            $date = \DateTime::createFromFormat('d/m/Y', $date);
            $date = $date->format($format);
        }
        return $date;
    }

    public function references(): ?string
    {
        return $this->metadata('references');
    }

    /**
     * @return string[]
     */
    public function warningFlags(): array
    {
        return $this->metadata('warning_flags', false);
    }

    public function getInventorName(): string
    {
        $inventor = $this->getInventor();

        if ($inventor === null) {
            return 'unknown';
        }

        return $inventor->title();
    }

    public function getCategoryName(): string
    {
        $categories = get_the_category($this->ID);
        if (!$categories) {
            return '';
        }

        $names = [];
        foreach ($categories as $category) {
            if ($category->parent) {
                $parentCategories = get_categories(['include' => [$category->parent]]);
                $names [] = $parentCategories[0]->name . ' &gt; ' . $category->name;
            } else {
                $names[] = $category->name;
            }
        }

        return implode(', ', $names);
    }

    protected function getInventor(): ?Inventor
    {
        return $this->connected(
            Inventor::postType(),
            true,
            [],
            false,
            static::TECH_INVENTOR_CONNECTION
        );
    }

    protected static function registerCategoryTaxonomy()
    {
        // See https://wordpress.stackexchange.com/questions/161788/how-to-modify-a-taxonomy-thats-already-registered
        $categoryArgs = get_taxonomy('category');

        // Allow for categories being admin-edited but don't generate broken View links; they don't have their own Page
        $categoryArgs->publicly_queryable = false;
        $categoryArgs->show_in_nav_menus = false;

        // Re-register category only on Technology posts
        register_taxonomy('category', Technology::postType(), (array) $categoryArgs);
    }

    public static function addCustomAdminColumns(ArrayHelper $helper)
    {
        $helper->insertAfter('title', 'type', 'Type');
        $helper->insertAfter('type', 'public_date', 'Public Date');
    }

    public function getCustomAdminColumnValue($column)
    {
        switch ($column) {
            case 'type':
                if ($this->isPatent()) {
                    return 'Patent';
                } else {
                    return 'Born Free';
                }
            case 'public_date':
                return $this->publicDate();
            default:
                return parent::getCustomAdminColumnValue($column);
        }
    }


}
