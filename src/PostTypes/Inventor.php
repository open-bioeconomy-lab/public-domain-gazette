<?php

namespace Outlandish\Website\PostTypes;

use Outlandish\Wordpress\Oowp\Util\ArrayHelper;

class Inventor extends BasePost
{
    public static function getRegistrationArgs()
    {
        $args = parent::getRegistrationArgs();
        $args['menu_icon'] = 'dashicons-groups';

        return $args;
    }

    protected function technologies($queryArgs = [])
    {
        return $this->connected(Technology::postType(), false, $queryArgs);
    }

    public static function addCustomAdminColumns(ArrayHelper $helper)
    {
        $helper->insertAfter('title', 'technologies', 'Technologies');
    }

    public function getCustomAdminColumnValue($column)
    {
        switch ($column) {
            case 'technologies':
                $tech = $this->technologies();
                return $tech->post_count;
            default:
                return parent::getCustomAdminColumnValue($column);
        }
    }


}
