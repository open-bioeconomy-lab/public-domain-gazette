<?php

namespace Outlandish\Website\Router;

use Outlandish\Website\Helpers\TechnologySearch;
use Outlandish\Website\PostTypes\BasePost;
use Outlandish\Website\PostTypes\FakePost;
use Outlandish\Website\PostTypes\Page;
use Outlandish\Website\PostTypes\RowBuilderPost;
use Outlandish\Website\PostTypes\Technology;
use Outlandish\Website\Response\RssResponse;
use Outlandish\Website\Views\Components\Banner;
use Outlandish\Website\Views\Components\Breadcrumb;
use Outlandish\Website\Views\Components\SearchForm;
use Outlandish\Website\Views\Pages\DefaultPost;
use Outlandish\Website\Views\Pages\FrontPage;
use Outlandish\Website\Views\Pages\SearchResultsPage;
use Outlandish\Website\Views\Pages\TechnologyPage;
use Outlandish\Website\Views\RowGroups\RelatedPostsRows;
use Outlandish\Website\Views\RowGroups\RowBuilderRows;
use Outlandish\Website\Views\Rows\LinksRow;
use Outlandish\Website\Views\Rows\WysiwygRow;
use Outlandish\Wordpress\Oowp\OowpQuery;
use Outlandish\Wordpress\Oowp\PostTypes\WordpressPost;
use Outlandish\Wordpress\Oowp\WordpressTheme;
use Outlandish\Wordpress\Routemaster\Oowp\OowpRouter;
use Outlandish\Wordpress\Routemaster\Oowp\View\SitemapView;
use Outlandish\Wordpress\Routemaster\Response\XmlResponse;
use WP_Term;

class OlRouter extends OowpRouter
{
    public function __construct()
    {
        parent::__construct(new OlRouterHelper());
        $this->addRoute('|^technology/(.+)/?$|i', 'technology');
        $this->addRoute('|^rss.xml$|i', 'rss');
        $this->addRoute('|^not-found/$|i', 'show404');
    }

    public function route()
    {
        // ignore any requests that post gravity form AJAX data
        if (isset($_POST['gform_ajax'])) {
            return;
        }
        parent::route();
    }

    protected function frontPage()
    {
        /** @var Page $frontPage */
        $frontPage = $this->helper->querySingle(
            ['page_id' => get_option('page_on_front')],
            true
        );

        $categories = [];
        foreach (get_categories(['parent' => 0]) as $parentCategory) {
            $subcategoriesForParent = get_categories(['parent' => $parentCategory->term_id]);
            if ($subcategoriesForParent) {
                $categories[$parentCategory->name] = $subcategoriesForParent;
            }
        }

        $search = new TechnologySearch($_GET);
        $searchForm = new SearchForm($search->getArgs(), $categories);

        if ($search->hasQuery()) {
            $search->findTechnologies();
            $searchTerm = $search->getSearchTerm();
            $post = new FakePost();
            $post->post_title = 'Search results' . ($searchTerm ? ": $searchTerm" : '');
            $post->setAsGlobal();
            return new SearchResultsPage($searchForm, $search);
        } else {
            /** @var Technology $featured */
            $featured = $frontPage->metadata('featured_technology');
            if ($featured) {
                $featured = WordpressPost::createWordpressPost($featured);
            }

            return new FrontPage(
                $frontPage->metadata('strapline'),
                $frontPage->content(),
                $featured,
                $searchForm
            );
        }
    }

    protected function defaultPost($slug)
    {
        /** @var BasePost $post */
        $post = $this->helper->querySingle(
            ['name' => $slug, 'post_type' => 'any'],
            true
        );

        $rowBuilderRows = null;
        $linkedPostIds = [];
        $relatedPostsRows = null;

        if ($post instanceof BasePost) {
            if ($post instanceof RowBuilderPost) {
                $rowBuilderContent = $post->getRowBuilderContent();
                $rowBuilderRows = new RowBuilderRows($rowBuilderContent);
                $linkedPostIds = $post->linkedPostIds;
            }
            $relatedPosts = $post->getRelatedPostsGroupedByType($linkedPostIds);
            $relatedPostsRows = new RelatedPostsRows($relatedPosts);
        }

        return new DefaultPost(
            new Banner(
                $post->title(),
                $post->featuredImage(),
                $post->featuredImageMobile(),
                $post->metadata('buttons')
            ),
            new Breadcrumb($post->breadcrumbTrail()),
            new WysiwygRow($post->content()),
            $rowBuilderRows,
            $relatedPostsRows
        );
    }

    protected function technology($slug)
    {
        /** @var Technology $technology */
        $technology = $this->helper->querySingle(
            ['name' => $slug, 'post_type' => Technology::postType()],
            true
        );

        return new TechnologyPage(
            $technology,
            new Breadcrumb($technology->breadcrumbTrail()),
            new WysiwygRow($technology->content()),
            new RelatedPostsRows($technology->getRelatedPostsGroupedByType())
        );
    }

    /**
     * Enforces a 404 response even if someone visits siteurl.com/not-found/ directly.
     */
    protected function show404()
    {
        $response = $this->helper->createNotFoundResponse();
        $response->setRouteName('404');
        $response->headers[] = 'HTTP/1.0 404 Not Found';
        return $response;
    }

    /**
     * @route /rss.xml
     */
    protected function rss()
    {
        return new RssResponse([
            'items' => Technology::fetchAll(['orderby' => 'date', 'order' => 'desc']),
            'name' => get_bloginfo('name'),
            'url' => WordpressTheme::getInstance()->homeURL()
        ]);
    }

    /**
     * Prevent the Not Found page appearing in the sitemap.
     *
     * @route /sitemap.xml
     */
    protected function sitemap()
    {
        $notFound = Page::fetchBySlug('not-found');
        $postsToHide = [];
        if ($notFound) {
            $postsToHide[] = $notFound->ID;
        }
        $view = new SitemapView(new OowpQuery([
            'post_type' => 'any',
            'orderby' => 'date',
            'post__not_in' => $postsToHide
        ]));
        return new XmlResponse($view);
    }
}
