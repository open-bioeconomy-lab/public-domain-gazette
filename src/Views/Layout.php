<?php

namespace Outlandish\Website\Views;

use Outlandish\Website\Views\Rows\CallToActionRow;
use Outlandish\Wordpress\Routemaster\Oowp\View\ContainerView;
use Outlandish\Website\PostTypes\BasePost;
use Outlandish\Website\Views\Components\Metadata;
use Outlandish\Website\Views\Components\Navbar;
use Outlandish\Website\Views\Components\Footer;
use Outlandish\Website\Views\Components\CookieBanner;
use Outlandish\Website\Views\Components\BackToTop;

class Layout extends ContainerView
{
    /** @var string - HTML outputted by wp_nav_menu() */
    public $menuItems;

    /** @var string */
    public $cookieNotification;

    /** @var string */
    public $siteTitle;

    /** @var array */
    public $callsToAction;

    /** @var array */
    public $footerElements;

    /** @var string */
    public $gtmId;

    public function render($args = [])
    {
        $postType = $this->post->postType();

        // If post password required and it doesn't match the cookie.
        $passwordRequired = post_password_required($this->post->get_post());

        $bodyClasses = [
            $postType,
            $postType . '-' . $this->post->ID,
            $postType . '-' . $this->post->post_name,
        ];

        if (is_front_page()) {
            $bodyClasses[] = 'front-page';
        }

        if (is_user_logged_in()) {
            $bodyClasses[] = 'wp-logged-in';
        }

        if (is_admin_bar_showing()) {
            $bodyClasses[] = 'wp-admin-bar';
            $bodyClasses[] = 'no-customize-support';
        }

        $description = '';
        $openGraphData = [];
        $twitterCardType = '';

        if ($this->post instanceof BasePost) {
            $title = $this->post->titleForMetadata();

            if (!$passwordRequired) {
                $description = $this->post->descriptionForMetadata();
                $openGraphData = $this->post->openGraphData();
                $twitterCardType = $this->post->twitterCardType();
            }
        } else {
            $title = $this->post->title();
        }

        if ($title) {
            $title .= ' | ';
        }
        $title .= $this->siteTitle;

        $metadata = new Metadata(
            $title,
            $this->post->robots(),
            $description,
            $openGraphData,
            $twitterCardType
        );

        $bodyClasses   = implode(' ', array_filter($bodyClasses));
        $navbar        = new Navbar($this->menuItems, $this->siteTitle, '/app/themes/outlandish/assets/img/pdg-logo.png');
        $callsToAction = new CallToActionRow($this->callsToAction);
        $footer        = new Footer($this->footerElements);
//        $backToTop     = new BackToTop();
        $cookieBanner  = $this->cookieNotification ? new CookieBanner($this->cookieNotification) : null;

        ?><!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php
        if ($this->gtmId) : ?>
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','<?php echo $this->gtmId; ?>');</script>
            <!-- End Google Tag Manager -->
        <?php endif;

        $metadata->render();
        wp_head();
        ?>
    </head>
    <body class="<?php echo $bodyClasses; ?>">
        <?php if ($this->gtmId) : ?>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $this->gtmId; ?>"
                              height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
        <?php endif; ?>

        <div class="main-wrapper">
            <?php $navbar->render(); ?>

            <main role="main">
                <?php if ($passwordRequired) : ?>
                    <div class="container post-main-container mt-4">
                        <?php echo get_the_password_form($this->post->get_post()); ?>
                    </div>
                <?php elseif ($this->content) :
                    $this->content->render();
                endif; ?>
            </main>

            <?php
            $callsToAction->render();
            $footer->render();
            ?>
        </div>

        <?php
        if ($cookieBanner) {
            $cookieBanner->render();
        }
        wp_footer();
        ?>
</body>
</html>
        <?php
    }
}
