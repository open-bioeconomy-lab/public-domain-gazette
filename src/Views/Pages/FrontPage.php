<?php

namespace Outlandish\Website\Views\Pages;

use Outlandish\Website\PostTypes\Technology;
use Outlandish\Website\Views\Components\Banner;
use Outlandish\Website\Views\Components\SearchForm;
use Outlandish\Website\Views\Rows\FeaturedTechnologyRow;
use Outlandish\Website\Views\Rows\LinksRow;
use Outlandish\Website\Views\Rows\WysiwygRow;
use Outlandish\Wordpress\Routemaster\Oowp\View\RoutemasterOowpView;

class FrontPage extends RoutemasterOowpView
{
    /** @var string */
    protected $strapline;

    /** @var Technology */
    protected $featuredTechnology;

    /** @var string */
    protected $mainContent;

    /** @var SearchForm */
    protected $searchForm;

    /**
     * @param string $strapline
     * @param string $mainContent
     * @param Technology|null $featuredTechnology
     * @param SearchForm $searchForm
     */
    public function __construct(
        $strapline,
        $mainContent,
        $featuredTechnology,
        $searchForm
    ) {
        parent::__construct(get_defined_vars());
    }

    public function render($args = [])
    {
        $banner = new Banner($this->strapline);
        $mainContent = new WysiwygRow($this->mainContent);
        $featuredRow = new FeaturedTechnologyRow($this->featuredTechnology, ['title' => 'This month\'s Featured Technology']);

        $banner->render();
        $mainContent->render();

        ?>
        <section class="search-technologies">
            <div class="container-fluid">
                <h2 class="content-row__title">Find a Technology</h2>
                <?php $this->searchForm->render(); ?>
            </div>
        </section>
        <?php

        $featuredRow->render();

        $recentTechRow = $this->getRecentTechRow();
        if ($recentTechRow) {
            $recentTechRow->render();
        }
    }

    protected function getRecentTechRow(): ?LinksRow
    {
        $recentlyAddedTech = Technology::fetchAll([
            'orderby' => 'date',
            'order' => 'desc',
            'posts_per_page' => 12,
        ]);

        if ($recentlyAddedTech->post_count === 0) {
            return null;
        }

        return new LinksRow(
            $recentlyAddedTech->posts,
            false,
            false,
            ['title' => 'Recently Added Technologies']
        );
    }
}
