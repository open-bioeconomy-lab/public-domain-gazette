<?php

namespace Outlandish\Website\Views\Pages;

use Outlandish\Website\Helpers\TechnologySearch;
use Outlandish\Website\Views\Components\SearchForm;
use Outlandish\Website\Views\Rows\LinksRow;
use Outlandish\Wordpress\Routemaster\Oowp\View\RoutemasterOowpView;

class SearchResultsPage extends RoutemasterOowpView
{
    /** @var TechnologySearch */
    protected $search;

    /** @var SearchForm */
    protected $searchForm;

    /**
     * @param SearchForm $searchForm
     * @param TechnologySearch $search
     */
    public function __construct($searchForm, $search)
    {
        parent::__construct(get_defined_vars());
    }

    public function render($args = [])
    {
        ?>
        <section class="search-technologies">
            <div class="container-fluid">
                <h2 class="content-row__title">Find a Technology</h2>
                <?php $this->searchForm->render(); ?>
            </div>
        </section>
        <?php

        $searchTerm = $this->search->getSearchTerm();
        $results = $this->search->getSearchResults();
        $resultsRow = null;
        if ($results) {
            $detail = $searchTerm ? " for <strong>$searchTerm</strong>" : '';
            if ($results->post_count) {
                $matches = ($results->post_count) === 1 ? 'match' : 'matches';
                $message = "Found $results->post_count $matches$detail";
                $resultsRow = new LinksRow($results->posts);
            } else {
                $message = "Sorry, no matches$detail. Do you want to try again?";
            }
        } else {
            $message = ''; // Likely followed menu link for faceted search -> no error
        }
        ?>
        <div class="container-fluid">
            <div class="content-row">
                <p class="body-text"><?php echo $message; ?></p>
                <?php
                if ($resultsRow) {
                    $resultsRow->render();
                }
                ?>
            </div>
        </div>
        <?php
    }
}
