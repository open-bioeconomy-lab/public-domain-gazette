<?php

namespace Outlandish\Website\Views\Pages;

use Outlandish\Website\PostTypes\Technology;
use Outlandish\Website\Views\Components\Breadcrumb;
use Outlandish\Website\Views\RowGroups\RelatedPostsRows;
use Outlandish\Website\Views\Rows\WysiwygRow;
use Outlandish\Wordpress\Routemaster\Oowp\View\RoutemasterOowpView;

class TechnologyPage extends RoutemasterOowpView
{
    /** @var Technology */
    protected $technology;

    /** @var Breadcrumb */
    protected $breadcrumb;

    /** @var WysiwygRow */
    protected $mainContent;

    /** @var RelatedPostsRows */
    protected $relatedPostsRows;

    public function __construct(
        Technology $technology,
        $breadcrumb,
        $mainContent,
        $relatedPostsRows
    ) {
        $this->technology = $technology;

        parent::__construct(compact(
            'breadcrumb',
            'mainContent',
            'relatedPostsRows'
        ));
    }

    public function render($args = [])
    {
        $this->breadcrumb->render();
        ?>

        <div class="container">
            <h1><?= $this->technology->title() ?></h1>

            <ul class="flags">
                <?php foreach ($this->technology->openUseFlags() as $openUseFlag) : ?>
                    <li class="badge badge-pill badge-secondary open-use-flag"><?= $openUseFlag ?></li>
                <?php endforeach; ?>

                <?php foreach ($this->technology->warningFlags() as $warningFlag) : ?>
                    <li class="badge badge-pill badge-warning"><?= $warningFlag ?></li>
                <?php endforeach; ?>
            </ul>

            <div class="row">
                <div class="col-md-6 col-lg-5">
                    <img src="<?= $this->technology->featuredImage('large') ?>" role="presentation" alt="">
                </div>
                <div class="col-md-6 col-lg-7">
                    <h2 class="content-row__subtitle">In the Public Domain since</h2>
                    <p class="technology-detail"><strong><?= $this->technology->publicDate() ?></strong></p>

                    <h2 class="content-row__subtitle">Description</h2>
                    <div class="technology-detail"><?php $this->mainContent->render(); ?></div>

                    <div class="row">
                        <div class="col-lg-7">
                            <?php if (!empty($this->technology->abstract())) : ?>
                                <h2 class="content-row__subtitle">Abstract</h2>
                                <p class="technology-detail"><?= $this->technology->abstract() ?></p>
                            <?php endif; ?>

                            <h2 class="content-row__subtitle">Category</h2>
                            <p id="technology-category" class="technology-detail"><?= $this->technology->getCategoryName() ?></p>

                            <h2 class="content-row__subtitle">Related Inventors</h2>
                            <p class="technology-detail"><?= $this->technology->getInventorName() ?></p>
                        </div>
                        <div class="col-lg-5">
                            <h2 class="content-row__subtitle">Source</h2>
                            <?php if ($this->technology->isPatent()) : ?>
                                <p class="technology-detail">US Patent No <?= $this->technology->patentNumber() ?></p>
                                <p class="technology-detail">Patent Title: <?= $this->technology->patentTitle() ?></p>

                                <?php if (!empty($this->technology->lensUrl())) : ?>
                                    <p class="technology-detail"><a href="<?= $this->technology->lensUrl() ?>" target="_blank">View on Lens <span class="fal fa-external-link"></span></a></p>
                                <?php endif; ?>

                                <?php if (!empty($this->technology->lensDnaSequenceUrl())) : ?>
                                    <p class="technology-detail"><a href="<?= $this->technology->lensDnaSequenceUrl() ?>" target="_blank">DNA sequence(s) on Lens <span class="fal fa-external-link"></span></a></p>
                                <?php endif; ?>

                                <?php if (!empty($this->technology->lensFamilyUrl())) : ?>
                                    <p class="technology-detail"><a href="<?= $this->technology->lensFamilyUrl() ?>" target="_blank">Patent family on Lens <span class="fal fa-external-link"></span></a></p>
                                <?php endif; ?>
                            <?php else : ?>
                                <p class="technology-detail">Born Free</p>
                            <?php endif; ?>

                            <?php if (!empty($this->technology->references())) : ?>
                                <div class="technology-detail"><?= $this->technology->references() ?></div>
                            <?php endif; ?>

                            <?php if (!empty($this->technology->footnote())) : ?>
                                <h2 class="content-row__subtitle">Footnotes</h2>
                                <p class="technology-detail"><?= $this->technology->footnote() ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->relatedPostsRows->render();
    }
}
