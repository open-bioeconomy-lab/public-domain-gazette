<?php

namespace Outlandish\Website\Views\Components;

use Outlandish\Wordpress\Routemaster\Oowp\View\RoutemasterOowpView;
use WP_Term;

class SearchForm extends RoutemasterOowpView
{
    /** @var string[] */
    protected $previousSearchTerms;

    /** @var WP_Term[]  Keyed on parent category label */
    protected $subcategories;

    public function __construct(array $previousSearchTerms, $subcategories)
    {
        parent::__construct(get_defined_vars());
    }

    public function render($modifier = '')
    {
        if ($modifier) {
            $modifier = 'search-form--' . $modifier;
        }

        ?>
        <form class="search-form <?php echo $modifier; ?>"
              method="GET"
              action="/"
              role="search">
            <div class="form-group form-group--category">
                <select id="category" name="category" aria-label="Limit to category" class="custom-select">
                    <option value="">Any category</option>
                    <?php foreach ($this->subcategories as $parentCategoryLabel => $childCategories) : ?>
                        <optgroup label="<?= $parentCategoryLabel ?>">
                            <?php foreach ($childCategories as $childCategory) : ?>
                                <option
                                    <?= $this->previousSearchTerms['category'] === $childCategory->slug ? 'selected' : '' ?>
                                    value="<?= $childCategory->slug ?>"
                                ><?= $childCategory->name ?></option>
                            <?php endforeach; ?>
                        </optgroup>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group form-group--type">
                <div class="custom-control custom-radio">
                    <input <?= $this->previousSearchTerms['type'] === 'bornfree' ? 'checked' : '' ?> type="radio" name="type" value="bornfree" id="typeBornfree" class="custom-control-input">
                    <label for="typeBornfree" class="custom-control-label">Born Free</label>
                </div>
                <div class="custom-control custom-radio">
                    <input <?= $this->previousSearchTerms['type'] === 'patent' ? 'checked' : '' ?> type="radio" name="type" value="patent" id="typePatent" class="custom-control-input">
                    <label for="typePatent" class="custom-control-label">Off Patent</label>
                </div>
                <div class="custom-control custom-radio">
                    <input <?= empty($this->previousSearchTerms['type']) ? 'checked' : '' ?> type="radio" name="type" value="" id="typeBoth" class="custom-control-input">
                    <label for="typeBoth" class="custom-control-label">Both</label>
                </div>
            </div>

            <div class="form-group form-group--search">
                <input
                    name="s"
                    class="form-control search-form__input"
                    type="search"
                    placeholder="Search Term"
                    value="<?= $this->previousSearchTerms['s'] ?? '' ?>"
                    aria-label="Search"
                >
            </div>

            <button class="btn btn-primary" type="submit" title="Search" id="search-button">
                <i class="fa fa-search"></i>
            </button>
        </form>
        <?php
    }
}
