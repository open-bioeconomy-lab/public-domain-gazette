<?php

namespace Outlandish\Website\Views\Components;

use Outlandish\Wordpress\Routemaster\Oowp\View\RoutemasterOowpView;

/**
 * Class LinkCard
 * @package Outlandish\Website\Views\Components
 */
class LinkCard extends RoutemasterOowpView
{
    /** @var string */
    protected $url;

    /** @var string */
    protected $image;

    /** @var string */
    protected $title;

    /** @var string */
    protected $caption;

    /** @var string|null    UV colour block to use in preference to an image */
    protected $colour;

    /** @var string[] */
    protected $openUseFlags;

    /** @var bool */
    protected $compact;

    /**
     * @param string        $url
     * @param string        $image
     * @param string        $title
     * @param string        $caption        Optional description of the item
     * @param string|null   $colour         Optional block of colour to show instead of an image
     * @param array         $openUseFlags   Optional open use flags to render below the title
     * @param bool          $compact
     */
    public function __construct($url, $image, $title, $caption = '', $colour = null, $openUseFlags = [], $compact = false)
    {
        parent::__construct(get_defined_vars());
    }

    public function render($label = '')
    {
        if ($this->colour) {
            $imageElement = '<div class="circle" style="background: ' . $this->colour . '"></div>';
        } elseif ($this->image) {
            $imageElement = '<img src="' . $this->image . '">';
        } else {
            $imageElement = '<div class="no-image"></div>';
        }
        $articleClass = 'link-card';
        if ($this->compact) {
            $articleClass .= ' link-card--compact';
        }
        ?>
        <a href="<?php echo $this->url; ?>" class="<?php echo $articleClass; ?>">
            <article>
                <div class="link-card-section link-card-section--image">
                    <?= $imageElement ?>
                </div>
                <div class="link-card-section link-card-section--info">
                    <h4 class="link-card__title"><?php echo $this->title; ?></h4>

                    <?php if ($this->openUseFlags) : ?>
                        <ul class="flags">
                            <?php foreach ($this->openUseFlags as $openUseFlag) : ?>
                                <li class="badge badge-pill badge-secondary"><?= $openUseFlag ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>

                    <?php if ($this->caption) : ?>
                        <div class="link-card__text">
                            <?php echo $this->caption; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </article>
        </a>
        <?php
    }
}
