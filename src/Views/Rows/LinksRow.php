<?php

namespace Outlandish\Website\Views\Rows;

use Outlandish\Website\PostTypes\Technology;
use Outlandish\Website\Views\Components\LinkCard;
use Outlandish\Website\PostTypes\BasePost;

class LinksRow extends Row
{
    const ROW_CLASS = 'links-row';

    /** @var bool */
    protected $compact;

    /** @var array */
    protected $linkedItems;

    /** @var bool */
    protected $showExcerpts;

    /** @var bool */
    protected $showOpenUseFlags;

    public function __construct(
        $linkedItems = [],
        $showExcerpts = false,
        $compact = false,
        $defaultArgs = []
    ) {
        parent::__construct($defaultArgs);

        $this->compact = $compact;
        $this->linkedItems = $linkedItems;
        $this->showExcerpts = $showExcerpts;
        $this->showOpenUseFlags = !$compact;
    }

    public function render($args = [])
    {
        if (empty($this->linkedItems)) {
            return;
        }

        parent::render($args);
    }


    protected function renderContent($showLabels = false)
    {
        ?>
        <div class="row">
            <?php foreach ($this->linkedItems as $linkedPost) :
                $colour = null;
                if ($linkedPost instanceof Technology) {
                    $colour = $linkedPost->uvColour();
                }

                /** @var BasePost|Technology $linkedPost */
                $linkCard = new LinkCard(
                    $linkedPost->permalink(),
                    $linkedPost->featuredImageThumbnail('medium'),
                    $linkedPost->title(),
                    $this->showExcerpts ? $linkedPost->excerpt(120) : null,
                    $colour,
                    $this->showOpenUseFlags ? $linkedPost->openUseFlags() : [],
                    $this->compact
                );
                ?>
                <div class="<?php echo static::getColumnClasses(3); ?>">
                    <?php
                    $linkCard->render($showLabels ? $linkedPost::friendlyName() : null);
                    ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?php
    }
}
