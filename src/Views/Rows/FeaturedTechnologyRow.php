<?php

namespace Outlandish\Website\Views\Rows;

use Outlandish\Website\PostTypes\Technology;
use Outlandish\Website\Views\Components\LinkCard;

class FeaturedTechnologyRow extends Row
{
    const ROW_CLASS = 'featured-technology-row';

    /** @var Technology */
    protected $technology;

    public function __construct(Technology $technology, $defaultArgs = [])
    {
        parent::__construct($defaultArgs);
        $this->technology = $technology;
    }

    protected function renderContent($args = [])
    {
        $featureContent = $this->technology->featureContent();
        if (!$featureContent) {
            $featureContent = $this->technology->abstract();
            if ($featureContent) {
                $featureContent = '<h4>Abstract</h4><p>' . $featureContent . '</p>';
            }
        }
        ?>
        <div class="row featured-tech">
            <div class="col-lg">
                <?php
                $linkCard = new LinkCard(
                    $this->technology->permalink(),
                    $this->technology->featuredImageThumbnail('medium'),
                    $this->technology->title(),
                    $this->technology->content(),
                    $this->technology->uvColour(),
                    $this->technology->openUseFlags()
                );
                $linkCard->render();
                ?>
            </div>
            <div class="col-lg feature-content">
                <?= $featureContent ?>
            </div>
        </div>
        <?php
    }
}
