![PDG Logo](/assets/img/pdg-logo.png "PDG Logo")

# Public Domain Gazette

## Visit the Public Domain Gazette at [publicdomaingazette.org](https://publicdomaingazette.org/)

## Background
The Public Domain Gazette enables biological engineers and life science researchers to discover useful, open technologies to accelerate innovation for improved global health.
PDG is an online resource seeded with information from patent offices and born-open initiatives that enables researchers to easily identify technologies that are freely available in the public domain. Descriptions of technologies are enhanced through summaries of how these useful tools can be used to accelerate research. By making these off-patent and born-open technologies easily discoverable and accessible, the PDG will:

 - Improve discovery of public domain technologies for life science researchers via an online portal;
 - Enhance the value of publicly accessible information through curation and crowdsourced domain expert knowledge of how open technologies can accelerate research and ultimately improve health;
 - Facilitate research on the public domain in the life sciences through providing downloadable datasets and enabling user research.

Effective use of technologies in the public domain is crucial to the growth and development of the global bioeconomy. Since the biotechnology revolution began in the early 1980’s, scientists in academia and industry have been patenting their inventions. Although the patent system is often associated with exclusivity and monopoly, it is actually one of the best mechanisms for building the public domain.

PDG was created as a response to the growing number of expired patents and increasing dedication of technologies to the public domain through initiatives such as OpenPlant, Structural Genomics Consortium, Gathering for Open Science Hardware, and more. By connecting researchers with off-patent and born-open technologies that are freely available in the public domain, PDG aims to accelerate discovery and innovation in the life sciences for improved global health.
