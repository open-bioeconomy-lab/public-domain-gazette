$(document).ready(() => {
  const inputField = $('.search-form__input');
  const submitButton = $('#search-button');

  function updateButton() {
    const hasCategoryConstraint = Boolean($('#category').val());  // 'Any Category' option value is a blank string.
    const hasTypeConstraint = !$('#typeBoth').is(':checked');     // One of the constraining radios is selected if 'Both' isn't.
    const hasTerm = Boolean(inputField.val());

    if (hasCategoryConstraint || hasTypeConstraint || hasTerm) {
      submitButton.removeAttr('disabled');
    } else {
      submitButton.attr('disabled', 'disabled');
    }
  }

  // Any form value change, which includes term changes but only on blur.
  $('.search-form select, .search-form input').change(updateButton);

  // For the term input field, detect changes immediately instead of waiting for blur.
  inputField.keyup(updateButton);

  // Sync submit button state with existing form state on page load.
  updateButton();
});
