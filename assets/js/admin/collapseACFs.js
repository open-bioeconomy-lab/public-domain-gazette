// Collapse some ACFs on load / after save, to keep the EDIT page neat
(function() {
  var $ = jQuery;
  // Flexible content rows (i.e. RowBuilder rows)
  $(".acf-flexible-content .layout").addClass("-collapsed");
})();
